$(document).ready(function () {
	init_validateForm();
	fullpageSlide();
	scrollToContact();
	imgContent();
	popover();
});

function imgContent() {
	const $imgContent = $('.img-content');
	$imgContent.height($imgContent.width());
	$(window).resize(function() {
		$imgContent.height($imgContent.width());
	});
}

function fullpageSlide() {
	const sm = $(window).width();
	if (sm >= 991) {
		$('#fullpage').fullpage({
			navigation: true,
			navigationPosition: 'right',
			navigationTooltips: [
				'Giới thiệu Hilton',
				'Chủ đầu tư dự án',
				'Thông tin dự án',
				'Vị thế đắc địa',
				'Đặc điểm nổi bật',
				'Tiện ích bên ngoài',
				'Tiện ích bên trong',
				'Chính sách ưu đãi',
				'Đăng ký đầu tư',
			],
			// 'showActiveTooltip': true,
			css3: true
		});
	}
}

function popover() {
	$('[data-toggle="popover"]').popover();
}

function init_validateForm() {
	$('.form__input').keydown(function () {
		$(this).removeClass('input-error');
		$('label[for="' + $(this).attr('id') + '"]').removeClass('text--red');
	});

	$('#contact').submit(() => {
		let name = validateName('.input--name');
		let phoneNumber = validatePhoneNumber('.input--phone-number');
		let email = validateEmail('.input--email');

		if (!name && !phoneNumber && !email) return false;

		return true;
	});
}

function validateName(element) {
	let $inputName = $(element);
	let name = $inputName.val().replace(/ /g, '');
	let checkName = /^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$/;

	if (!checkName.test(name)) {
		$inputName.addClass('input-error');
		$('label[for="' + $inputName.attr('id') + '"]').addClass('text--red');

		return false;
	}

	return true;
}

function validateEmail(element) {
	let $inputEmail = $(element);
	let email = $inputEmail.val().trim();
	let checkEmail = /^[a-z0-9][a-z0-9.]{4,28}[a-z0-9]@[a-z]+\.[a-z.]{1,9}[a-z]$/i;
	let checkDots = /[.]{2,}/;
	let moveUser = email.substring(0, email.indexOf('@'));
	let checkCharacter = /[a-z]/i;

	if (!email) return true;

	if (!(checkEmail.test(email) && !checkDots.test(email) && checkCharacter.test(moveUser))) {
		$inputEmail.addClass('input-error');
		$('label[for="' + $inputEmail.attr('id') + '"]').addClass('text--red');

		return false;
	}

	return true;
}

function validatePhoneNumber(element) {
	let $inputPhoneNumber = $(element);
	let number = $inputPhoneNumber.val().replace(/ /g, '');
	let checkNumber0 = /^09[0-46-9][0-9]{7}$|^012[0-9]{8}$|^016[2-9][0-9]{7}$|^018[68][0-9]{7}$|^0199[0-9]{7}$|^08[689][0-9]{7}$|^02[0-9]{9}$/;
	let checkNumber84 = /^\+849[0-46-9][0-9]{7}$|^\+8412[0-9]{8}$|^\+8416[2-9][0-9]{7}$|^\+8418[68][0-9]{7}$|^\+84199[0-9]{7}$|^\+848[689][0-9]{7}$|^\+842[0-9]{9}$/;
	if (!checkNumber0.test(number) && !checkNumber84.test(number)) {
		$inputPhoneNumber.addClass('input-error');
		$('label[for="' + $inputPhoneNumber.attr('id') + '"]').addClass('text--red');
		return false;
	}

	return true;
}

function scrollToContact() {
	$(".btn--size, .btn__subscribe").click(function() {
		const sm = $(window).width();
		if (sm >= 991) {
			location.hash = "#lien-he";
		} else {
			$("html, body").animate({ scrollTop: $(".info").offset().top - 500 }, 900);
		}
	});
}