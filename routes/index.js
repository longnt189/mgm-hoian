const express = require('express');
const router = express.Router();

const { subscribe } = require('../controllers');

router.use('/robots.txt', require('./robots_txt'));
router.use('/auth', require('./auth'));
router.use('/admin', require('./administrator'));

router.post('/subscribe', (req, res, next) => {
	subscribe(req, res, next);
});

router.get('/thank-you', (req, res) => {
	res.render('client/thankyou');
});

router.get('/', (req, res) => {
	res.render('client/index');
});

router.use((req, res, next) => {
	const err = new Error('Not Found');
	err.status = 404;
	next(err);
});

router.use('/admin', (err, req, res, next) => {
	res.locals.message = err.message;

	res.status(err.status || 404);
	res.render('admin/404');
});

router.use((err, req, res, next) => {
	res.locals.message = err.message;

	res.status(err.status || 404);

	// res.status(404).send('404 - Not Found');

	res.status(404).render('client/404');
});

module.exports = router;
