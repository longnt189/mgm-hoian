module.exports = {
	checkPagination: (req, currentPage = 1) => {
		const { page } = req.query;

		if (page <= 0 || typeof page === 'undefined') {
			currentPage = 1;
		} else {
			currentPage = Math.ceil(parseInt(page));
		}

		return currentPage;
	},
	checkPaginationSearch: (req, currentPage) => {
		const { page } = req.query;

		if (page <= 0 || typeof page === 'undefined') {
			currentPage = 1;
		} else {
			currentPage = Math.ceil(parseInt(page));
		}

		req.getUrl = req.protocol + "://" + req.get('host') + req.originalUrl;
		const indexSearch = req.getUrl.indexOf('?');
		const indexPaginate = req.getUrl.indexOf('&page=') !== -1 ? req.getUrl.indexOf('&page=') : 999;
		req.searchUrl = req.getUrl.substring(indexSearch, indexPaginate);
	}
};
