const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const Administrator = require('../models/Administrators');

passport.use('local', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, (req, email, password, done) => {
	Administrator.findOne({email}, (err, user) => {
        if (err) return done(err);
        if (!user || !user.comaprePassword(password)) {
            return done(null, false, req.flash('error', 'Sai tên đăng nhập hoặc mật khẩu!'));
        }
        return done(null, user);
    });
}));

passport.serializeUser((user, done) => {
    done(null, user._id);
});

passport.deserializeUser((id, done) => {
	Administrator.findById(id, (err, user) => {
        done(err, user);
    });
});

module.exports.isAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    }
    return res.redirect('/auth');
};
