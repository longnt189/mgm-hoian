function fullpageSlideMarina() {
	const sm = $(window).width();
	if (sm >= 991) {
		$('#fullpageMarina').fullpage({
			navigation: true,
			navigationPosition: 'right',
			navigationTooltips: [
				'Tuần Châu Marina',
				'Thông tin dự án',
				'Vị thế đắc địa',
                'Tiền năng',
                'Loại hình căn hộ',
				'Đặc điểm nổi bật',
				'Tiện ích bên ngoài',
				'Tiện ích bên trong',
				'Chính sách ưu đãi',
				'Đăng ký đầu tư',
			],
			// 'showActiveTooltip': true,
			css3: true
		});
	}
}

$(document).ready(function() {
    fullpageSlideMarina();
})